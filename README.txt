This module is designed to add to the Opigno LMS distribution some 
'guided tour' feature, allowing new users to get started faster.

It depends on the guideme Drupal module.

To get started simply activate the module, and let it guide you through your 
first steps with Opigno!

For more information please visit: www.opigno.org
